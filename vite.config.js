import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [sveltekit()],
	server:{
		proxy: {
			'/mail': 'https://audrey-sage-education-canine.com'
		}
	}
});
